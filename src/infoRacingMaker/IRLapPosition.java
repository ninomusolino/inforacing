package infoRacingMaker;

import com.google.gson.annotations.SerializedName;

public class IRLapPosition 
{
	public int lap;
	@SerializedName("lewis-hamilton")
	public int lewishamilton;
	@SerializedName("nico-rosberg")
	public int nicorosberg;
	@SerializedName("daniel-ricciardo")
	public int danielricciardo;
	@SerializedName("daniil-kvyat")
	public int daniilkvyat;
	@SerializedName("felipe-massa")
	public int felipemassa;
	@SerializedName("valtteri-bottas")
	public int valtteribottas;
	@SerializedName("sebastian-vettel")
	public int sebastianvettel;
	@SerializedName("kimi-raikkonen")
	public int kimiraikkonen;
	@SerializedName("jenson-button")
    public int jensonbutton;
	@SerializedName("fernando-alonso")
    public int fernandoalonso;
	@SerializedName("sergio-perez")
    public int sergioperez;
	@SerializedName("nico-hulkenberg")
    public int nicohulkenberg;
	@SerializedName("max-verstappen")
	public int maxverstappen;
	@SerializedName("carlos-sainz")
    public int carlossainz;
	@SerializedName("romain-grosjean")
	public int romaingrosjean;
	@SerializedName("pastor-maldonado")
	public int pastormaldonado;
	@SerializedName("felipe-nasr")
	public int felipenasr;
	@SerializedName("marcus-ericsson")
	public int marcusericsson;
	@SerializedName("will-stevens")
    public int willstevens;
	@SerializedName("roberto-merhi")
	public int robertomerhi;
	
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap = lap;
	}
	public int getLewishamilton() {
		return lewishamilton;
	}
	public void setLewishamilton(int lewishamilton) {
		this.lewishamilton = lewishamilton;
	}
	public int getNicorosberg() {
		return nicorosberg;
	}
	public void setNicorosberg(int nicorosberg) {
		this.nicorosberg = nicorosberg;
	}
	public int getSebastianvettel() {
		return sebastianvettel;
	}
	public void setSebastianvettel(int sebastianvettel) {
		this.sebastianvettel = sebastianvettel;
	}
	public int getFelipemassa() {
		return felipemassa;
	}
	public void setFelipemassa(int felipemassa) {
		this.felipemassa = felipemassa;
	}
	public int getValtteribottas() {
		return valtteribottas;
	}
	public void setValtteribottas(int valtteribottas) {
		this.valtteribottas = valtteribottas;
	}
	public int getKimiraikkonen() {
		return kimiraikkonen;
	}
	public void setKimiraikkonen(int kimiraikkonen) {
		this.kimiraikkonen = kimiraikkonen;
	}
	public int getDanielricciardo() {
		return danielricciardo;
	}
	public void setDanielricciardo(int danielricciardo) {
		this.danielricciardo = danielricciardo;
	}
	public int getRomaingrosjean() {
		return romaingrosjean;
	}
	public void setRomaingrosjean(int romaingrosjean) {
		this.romaingrosjean = romaingrosjean;
	}
	public int getFelipenasr() {
		return felipenasr;
	}
	public void setFelipenasr(int felipenasr) {
		this.felipenasr = felipenasr;
	}
	public int getMarcusericsson() {
		return marcusericsson;
	}
	public void setMarcusericsson(int marcusericsson) {
		this.marcusericsson = marcusericsson;
	}
	public int getPastormaldonado() {
		return pastormaldonado;
	}
	public void setPastormaldonado(int pastormaldonado) {
		this.pastormaldonado = pastormaldonado;
	}
	public int getDaniilkvyat() {
		return daniilkvyat;
	}
	public void setDaniilkvyat(int daniilkvyat) {
		this.daniilkvyat = daniilkvyat;
	}
	public int getMaxverstappen() {
		return maxverstappen;
	}
	public void setMaxverstappen(int maxverstappen) {
		this.maxverstappen = maxverstappen;
	}
	public int getCarlossainz() {
		return carlossainz;
	}
	public void setCarlossainz(int carlossainz) {
		this.carlossainz = carlossainz;
	}
	public int getSergioperez() {
		return sergioperez;
	}
	public void setSergioperez(int sergioperez) {
		this.sergioperez = sergioperez;
	}
	public int getNicohulkenberg() {
		return nicohulkenberg;
	}
	public void setNicohulkenberg(int nicohulkenberg) {
		this.nicohulkenberg = nicohulkenberg;
	}
	public int getJensonbutton() {
		return jensonbutton;
	}
	public void setJensonbutton(int jensonbutton) {
		this.jensonbutton = jensonbutton;
	}
	public int getFernandoalonso() {
		return fernandoalonso;
	}
	public void setFernandoalonso(int fernandoalonso) {
		this.fernandoalonso = fernandoalonso;
	}
	public int getWillstevens() {
		return willstevens;
	}
	public void setWillstevens(int willstevens) {
		this.willstevens = willstevens;
	}
	public int getRobertomerhi() {
		return robertomerhi;
	}
	public void setRobertomerhi(int robertomerhi) {
		this.robertomerhi = robertomerhi;
	}
}
