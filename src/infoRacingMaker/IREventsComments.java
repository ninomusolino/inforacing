package infoRacingMaker;

import com.google.gson.annotations.SerializedName;

public class IREventsComments 
{
	public int lap;
	@SerializedName("lewis-hamilton")
	public String lewishamilton;
	@SerializedName("nico-rosberg")
	public String nicorosberg;
	@SerializedName("daniel-ricciardo")
	public String danielricciardo;
	@SerializedName("daniil-kvyat")
	public String daniilkvyat;
	@SerializedName("felipe-massa")
	public String felipemassa;
	@SerializedName("valtteri-bottas")
	public String valtteribottas;
	@SerializedName("sebastian-vettel")
	public String sebastianvettel;
	@SerializedName("kimi-raikkonen")
	public String kimiraikkonen;
	@SerializedName("jenson-button")
    public String jensonbutton;
	@SerializedName("fernando-alonso")
    public String fernandoalonso;
	@SerializedName("sergio-perez")
    public String sergioperez;
	@SerializedName("nico-hulkenberg")
    public String nicohulkenberg;
	@SerializedName("max-verstappen")
	public String maxverstappen;
	@SerializedName("carlos-sainz")
    public String carlossainz;
	@SerializedName("romain-grosjean")
	public String romaingrosjean;
	@SerializedName("pastor-maldonado")
	public String pastormaldonado;
	@SerializedName("felipe-nasr")
	public String felipenasr;
	@SerializedName("marcus-ericsson")
	public String marcusericsson;
	@SerializedName("will-stevens")
    public String willstevens;
	@SerializedName("roberto-merhi")
	public String robertomerhi;
    public String RACE;
    
	public int getLap() {
		return lap;
	}
	public void setLap(int lap) {
		this.lap = lap;
	}
	public String getLewishamilton() {
		return lewishamilton;
	}
	public void setLewishamilton(String lewishamilton) {
		this.lewishamilton = lewishamilton;
	}
	public String getNicorosberg() {
		return nicorosberg;
	}
	public void setNicorosberg(String nicorosberg) {
		this.nicorosberg = nicorosberg;
	}
	public String getSebastianvettel() {
		return sebastianvettel;
	}
	public void setSebastianvettel(String sebastianvettel) {
		this.sebastianvettel = sebastianvettel;
	}
	public String getFelipemassa() {
		return felipemassa;
	}
	public void setFelipemassa(String felipemassa) {
		this.felipemassa = felipemassa;
	}
	public String getValtteribottas() {
		return valtteribottas;
	}
	public void setValtteribottas(String valtteribottas) {
		this.valtteribottas = valtteribottas;
	}
	public String getKimiraikkonen() {
		return kimiraikkonen;
	}
	public void setKimiraikkonen(String kimiraikkonen) {
		this.kimiraikkonen = kimiraikkonen;
	}
	public String getDanielricciardo() {
		return danielricciardo;
	}
	public void setDanielricciardo(String danielricciardo) {
		this.danielricciardo = danielricciardo;
	}
	public String getRomaingrosjean() {
		return romaingrosjean;
	}
	public void setRomaingrosjean(String romaingrosjean) {
		this.romaingrosjean = romaingrosjean;
	}
	public String getFelipenasr() {
		return felipenasr;
	}
	public void setFelipenasr(String felipenasr) {
		this.felipenasr = felipenasr;
	}
	public String getMarcusericsson() {
		return marcusericsson;
	}
	public void setMarcusericsson(String marcusericsson) {
		this.marcusericsson = marcusericsson;
	}
	public String getPastormaldonado() {
		return pastormaldonado;
	}
	public void setPastormaldonado(String pastormaldonado) {
		this.pastormaldonado = pastormaldonado;
	}
	public String getDaniilkvyat() {
		return daniilkvyat;
	}
	public void setDaniilkvyat(String daniilkvyat) {
		this.daniilkvyat = daniilkvyat;
	}
	public String getMaxverstappen() {
		return maxverstappen;
	}
	public void setMaxverstappen(String maxverstappen) {
		this.maxverstappen = maxverstappen;
	}
	public String getCarlossainz() {
		return carlossainz;
	}
	public void setCarlossainz(String carlossainz) {
		this.carlossainz = carlossainz;
	}
	public String getSergioperez() {
		return sergioperez;
	}
	public void setSergioperez(String sergioperez) {
		this.sergioperez = sergioperez;
	}
	public String getNicohulkenberg() {
		return nicohulkenberg;
	}
	public void setNicohulkenberg(String nicohulkenberg) {
		this.nicohulkenberg = nicohulkenberg;
	}
	public String getJensonbutton() {
		return jensonbutton;
	}
	public void setJensonbutton(String jensonbutton) {
		this.jensonbutton = jensonbutton;
	}
	public String getFernandoalonso() {
		return fernandoalonso;
	}
	public void setFernandoalonso(String fernandoalonso) {
		this.fernandoalonso = fernandoalonso;
	}
	public String getWillstevens() {
		return willstevens;
	}
	public void setWillstevens(String willstevens) {
		this.willstevens = willstevens;
	}
	public String getRobertomerhi() {
		return robertomerhi;
	}
	public void setRobertomerhi(String robertomerhi) {
		this.robertomerhi = robertomerhi;
	}
	public String getRACE() {
		return RACE;
	}
	public void setRACE(String rACE) {
		RACE = rACE;
	}
}
