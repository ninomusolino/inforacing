package infoRacingMaker;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

public class IRTableModel extends AbstractTableModel{

	/**
	 * 
	 */
	public interface Reorderable 
	{
		   public void reorder(int fromIndex, int toIndex);
	}
	private static final long serialVersionUID = -3513890012084612521L;
	private Lap currentLap;
	
	public IRTableModel(Lap lap){
		this.currentLap = lap;
	}

	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return 21;
	}

	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 5;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) 
	{	
		//System.out.println ("TEST "+rowIndex);
		Driver	driver = new Driver();
		if(rowIndex<20)
		{
			if(currentLap.drivers[rowIndex]!= null)
				driver = currentLap.drivers[rowIndex];
		}
		switch(columnIndex)
		{
		case 0:
			if(rowIndex<20)
			{
				return driver.position;
			}
			else
				return "Race Status";
		case 1:
			if(rowIndex<20)
			{
				if(driver.name!= null)
					return driver.name;
				else
					return "";
			}
			else
				return "///////";
		case 2:
			if(rowIndex<20)
			{
				if(driver.events!= null)
					return driver.events;
				else
					return "";
			}
			else
				return currentLap.statusEvents;
		case 3:
			if(rowIndex<20)
			{
				if(driver.commentsIta!= null)
					return driver.commentsIta;
				else
					return "";
			}
			else
				return currentLap.statusIta;
		case 4:
			if(rowIndex<20)
			{
				if(driver.commentsEng!= null)
					return driver.commentsEng;
				else
					return "";
			}
			else
				return currentLap.statusEng;
		default:
			return "";
		}
		
		//return "";
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return super.getColumnClass(columnIndex);
	}

	@Override
	public void setValueAt(Object aValue, int rowIndex, int columnIndex) 
	{
		String value = ""+aValue;
		//System.out.println("chiamata "+value);
		//System.out.println(columnIndex);
		if(rowIndex<20)
		{
			switch(columnIndex)
			{
			case 0:
				currentLap.drivers[rowIndex].position = Integer.parseInt(value);
				break;
			case 1:
				currentLap.drivers[rowIndex].name = value;
				break;
			case 2:
				//System.out.println("Test "+value);
				currentLap.drivers[rowIndex].events = value;
				break;
			case 3:
				currentLap.drivers[rowIndex].commentsIta = value;
				//System.out.println("Test "+currentLap.drivers[rowIndex].commentsIta);
				break;
			case 4:
				currentLap.drivers[rowIndex].commentsEng = value;
				//System.out.println("Test "+currentLap.drivers[rowIndex].commentsEng);
				break;
			}
		}
		else
		{
			if(columnIndex == 2)
				currentLap.statusEvents = value;
			else if(columnIndex == 3)
				currentLap.statusIta = value;
			else if (columnIndex == 4)
				currentLap.statusEng = value;
			//System.out.println("Test "+ currentLap.statusEvents);
		}
	}

	
	public void shiftPositionUP (String value,int row)
	{
		List<String>driverName = new ArrayList<String>();
		for (int i=row ;i<20;i++)
		{
			if(!currentLap.drivers[i].name.equals(value))
			{
				//System.out.println("Altri piloti");
				driverName.add(currentLap.drivers[i].name);
			}
			else 
			{
				//System.out.println("PILOTA DA SPOSTARE");
			}
		}
		currentLap.drivers[row].name = value;
		int index = 0;
		for(int j = row+1 ;j<20;j++)
		{
			currentLap.drivers[j].name = driverName.get(index);
			index++;
		}
		this.fireTableDataChanged();
	}
	
	public void shiftPositionDOWN(String data, int row) 
	{
		List<String>driverName = new ArrayList<String>();
		for (int i=row ;i>=0;i--)
		{
			if(!currentLap.drivers[i].name.equals(data))
			{
				//System.out.println("Altri piloti");
				driverName.add(currentLap.drivers[i].name);
			}
			else 
			{
				//System.out.println("PILOTA DA SPOSTARE GIU");
			}
		}
		currentLap.drivers[row].name = data;
		int index = 0;
		for(int j = row-1 ;j>=0;j--)
		{
			currentLap.drivers[j].name = driverName.get(index);
			index++;
		}
		this.fireTableDataChanged();
		
	}
	
	
	@Override
	public boolean isCellEditable(int rowIndex, int columnIndex) 
	{
		if (columnIndex>0) 
			if (rowIndex == 20 && columnIndex>1)
			{	
				//System.out.println("row = "+rowIndex+" column = "+columnIndex);
				return true;
			}
			else if (rowIndex<20)
			{
				//System.out.println("row = "+rowIndex+" column = "+columnIndex);
				return true;
			}
			else
			{
				//System.out.println("row = "+rowIndex+" column = "+columnIndex);
				return false;
			}
		return false;
	}
	
	@Override
	public String getColumnName(int column) 
	{
		switch(column)
		{
		case 0:
			return "Posizione";
		case 1:
			return "Pilota";
		case 2:
			return "Eventi";
		case 3:
			return "Commenti Ita";
		case 4:
			return "Commenti Eng";
		default:
			return "";
		}
	}

	public Lap getCurrentLap() 
	{
		return currentLap;
	}

	public void setCurrentLap(Lap currentLap) 
	{
		this.currentLap = currentLap;
	}

	
	

}
