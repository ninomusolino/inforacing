package infoRacingMaker.UnitTest;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import infoRacingMaker.Driver;
import infoRacingMaker.IREventsComments;
import infoRacingMaker.IRJsonWriter;
import infoRacingMaker.IRLapPosition;
import infoRacingMaker.Lap;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IRJsonWriterTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testIRWritePosition() 
	{
		List<IRLapPosition> dataset  = new ArrayList<IRLapPosition>();
		Lap [] gara = new Lap [2];
		Driver [] drivers = new Driver[2];
		drivers[0] = new Driver("Hamilton", 20, "", "", "");
		drivers[1] = new Driver("Vettel", 1, "", "","");
		gara[0] = new Lap(drivers, "", "", null, 0);
		Driver [] drivers2 = new Driver[2];
		drivers2[0] = new Driver("Hamilton", 1, "", "", "");
		drivers2[1] = new Driver("Vettel", 20, "", "","");
		gara[1] = new Lap(drivers2,"","", null, 1);
		for(int i = 0; i < gara.length; i++)
		{
			IRLapPosition positionLap = new IRLapPosition();
			positionLap = IRJsonWriter.setDriverPosition(positionLap, gara[i]);
			dataset.add(positionLap);
		}
		IRLapPosition Lap0 = new IRLapPosition();
		IRLapPosition Lap1 = new IRLapPosition();
		Lap0 = dataset.get(0);
		Lap1 = dataset.get(1);
		assertEquals(1, Lap0.sebastianvettel);
		assertEquals(20, Lap0.lewishamilton);
		assertEquals(20, Lap0.lewishamilton);
		assertEquals(1, Lap1.lewishamilton);
		assertEquals(20, Lap1.sebastianvettel);
	}
	
	@Test
	public void testIRWriteEvent() 
	{
		List<IREventsComments> dataset  = new ArrayList<IREventsComments>();
		Lap [] gara = new Lap [2];
		Driver [] drivers = new Driver[2];
		drivers[0] = new Driver("Hamilton", 20, "", "Pit", "");
		drivers[1] = new Driver("Vettel", 1, "", "Orf", "");
		gara[0] = new Lap(drivers, "", "", null, 0);
		Driver [] drivers2 = new Driver[2];
		drivers2[0] = new Driver("Hamilton", 1, "", "Cane","");
		drivers2[1] = new Driver("Vettel", 20, "", "Gatto","");
		gara[1] = new Lap(drivers2,"","",null, 1);
		for(int i = 0; i < gara.length; i++)
		{
			IREventsComments positionLap = new IREventsComments();
			positionLap = IRJsonWriter.setEventDriver(positionLap, gara[i]);
			dataset.add(positionLap);
			positionLap = null;
		}
		IREventsComments Lap0 = new IREventsComments();
		IREventsComments Lap1 = new IREventsComments();
		Lap0 = dataset.get(0);
		Lap1 = dataset.get(1);
		assertEquals("Orf", Lap0.sebastianvettel);
		assertEquals("Pit", Lap0.lewishamilton);
		assertEquals("Cane", Lap1.lewishamilton);
		assertEquals("Gatto", Lap1.sebastianvettel);
	}
	

}
