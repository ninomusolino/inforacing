package infoRacingMaker.UnitTest;

import static org.junit.Assert.*;
import infoRacingMaker.Driver;
import infoRacingMaker.Lap;
import org.junit.Test;

public class TestLap {
	
	@Test
	public void testSetDrivers() 
	{	
		Lap testSet = new Lap();
		Driver drivers[] = new Driver[1];
		drivers[0] = new Driver("hamilton", 1, "", "","");
		testSet.setDrivers(drivers);
		assertArrayEquals(drivers, testSet.drivers);
	}

	@Test
	public void testGetStatus() 
	{
		Lap test = new Lap();
		test.statusIta = "test";
		assertEquals("test", test.getStatusIta());
	}
	
	@Test
	public void testGetCurrentLap() 
	{
		Lap test = new Lap(null, "","","", 1);
		assertEquals(1, test.getCurrentLap());
		
	}

}
