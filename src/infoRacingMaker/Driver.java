package infoRacingMaker;

public class Driver 
{
	public String name;
	public int position;
	public String commentsIta;
	public String commentsEng;
	public String events;
	
	public Driver()
	{
		name = "";
		position = 0;
		commentsIta = "";
		commentsEng = "";
		events = "";
	}

	public Driver(String name, int position, String commentsIta,String commentsEng, String events) 
	{
		super();
		this.name = name;
		this.position = position;
		this.commentsIta = commentsIta;
		this.commentsEng = commentsEng;
		this.events = events;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	public String getCommentsIta() {
		return commentsIta;
	}

	public void setCommentsIta(String commentsIta) {
		this.commentsIta = commentsIta;
	}

	public String getCommentsEng() {
		return commentsEng;
	}

	public void setCommentsEng(String commentsEng) {
		this.commentsEng = commentsEng;
	}

	public String getEvents() {
		return events;
	}

	public void setEvents(String events) {
		this.events = events;
	}	

}
