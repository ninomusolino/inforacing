package infoRacingMaker;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.*;

public class IRJsonWriter 
{

	public static void printIRLapPosition(Lap[] gara, String workingDirectory) throws IOException
	{
		List<IRLapPosition> dataset  = new ArrayList<IRLapPosition>();
		for(int i = 0; i < gara.length; i++)
		{
			IRLapPosition positionLap = new IRLapPosition();
			positionLap = setDriverPosition(positionLap,gara[i]);
			positionLap.lap = gara[i].currentLap;
			dataset.add(positionLap);
		}
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.setPrettyPrinting().create();
		//System.out.println(gson.toJson(dataset)); 
		File statText = new File(workingDirectory+"03.config.race.js");
        FileOutputStream is = new FileOutputStream(statText);
        OutputStreamWriter osw = new OutputStreamWriter(is);    
        Writer w = new BufferedWriter(osw);
        w.write("; $(document).ready(function() { if(raceReport__body!==null) {"
        		+ "raceReport__config_lapAndPositions ="+gson.toJson(dataset)+";}});");
        w.close();
	}

	public static void printIREvents(Lap[] gara, String workingDirectory) throws IOException
	{
		List<IREventsComments> dataset  = new ArrayList<IREventsComments>();
		for(int i = 0; i < gara.length; i++)
		{
			IREventsComments eventLap = new IREventsComments();
			eventLap = setEventDriver(eventLap,gara[i]);
			eventLap.lap = gara[i].currentLap;
			eventLap.RACE = gara[i].statusEvents;
			dataset.add(eventLap);
		}
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.setPrettyPrinting().create();
		
		//WRITING ITA JS
		File statText = new File(workingDirectory+"04.config.events.js");
        FileOutputStream is = new FileOutputStream(statText);
        OutputStreamWriter osw = new OutputStreamWriter(is);    
        Writer w = new BufferedWriter(osw);
        w.write("; $(document).ready(function() { if(raceReport__body!==null) {"
        		+ "raceReport__config_lapAndEvents = "+gson.toJson(dataset)+";}});");
        w.close();
	}

	public static void printIRComments(Lap[] gara, String workingDirectory) throws IOException
	{
		List<IREventsComments> datasetIta  = new ArrayList<IREventsComments>();
		List<IREventsComments> datasetEng  = new ArrayList<IREventsComments>();
		
		for(int i = 0; i < gara.length; i++)
		{
			IREventsComments commentLapIta = new IREventsComments();
			IREventsComments commentLapEng = new IREventsComments();
			
			commentLapIta = setCommentDriverIta(commentLapIta,gara[i]);
			commentLapIta.lap = gara[i].currentLap;
			commentLapIta.RACE = gara[i].statusIta;
			
			commentLapEng = setCommentDriverEng(commentLapEng, gara[i]);
			commentLapEng.lap = gara[i].currentLap;
			commentLapEng.RACE = gara[i].statusEng;
			
			datasetIta.add(commentLapIta);
			datasetEng.add(commentLapEng);
		}
		GsonBuilder builder = new GsonBuilder();
		Gson gson = builder.setPrettyPrinting().create();
		
		//WRITING ITA FILE
		File statText = new File(workingDirectory+"05.config.comments.js");
        FileOutputStream is = new FileOutputStream(statText);
        OutputStreamWriter osw = new OutputStreamWriter(is);    
        Writer w = new BufferedWriter(osw);
        w.write("; $(document).ready(function() { if(raceReport__body!==null) {"
        		+ "raceReport__config_lapAndComments = "+gson.toJson(datasetIta)+";}});");
        w.close();
        
        //WRITING ENG FILE
        statText = new File(workingDirectory+"05.config.comments_ENG.js");
        is = new FileOutputStream(statText);
        osw = new OutputStreamWriter(is);    
        w = new BufferedWriter(osw);
        w.write("; $(document).ready(function() { if(raceReport__body!==null) {"
        		+ "raceReport__config_lapAndComments = "+gson.toJson(datasetEng)+";}});");
        w.close();
	}
	
	public static IREventsComments setCommentDriverIta(IREventsComments commentLap, Lap lap) 
	{
		for(int j=0; j<lap.drivers.length;j++)
		{
			switch (lap.drivers[j].name) 
			{
			case "Hamilton":
				commentLap.lewishamilton = lap.drivers[j].commentsIta;
				break;
			case "Rosberg":
				commentLap.nicorosberg = lap.drivers[j].commentsIta;
				break;
			case "Ricciardo":
				commentLap.danielricciardo = lap.drivers[j].commentsIta;
				break;
			case "Kvyat":
				commentLap.daniilkvyat = lap.drivers[j].commentsIta;
				break;
			case "Massa":
				commentLap.felipemassa = lap.drivers[j].commentsIta;
				break;
			case "Bottas":
				commentLap.valtteribottas = lap.drivers[j].commentsIta;
				break;
			case "Vettel":
				commentLap.sebastianvettel = lap.drivers[j].commentsIta;
				break;
			case "Raikkonen":
				commentLap.kimiraikkonen = lap.drivers[j].commentsIta;
				break;
			case "Button":
				commentLap.jensonbutton = lap.drivers[j].commentsIta;
				break;
			case "Alonso":
				commentLap.fernandoalonso = lap.drivers[j].commentsIta;
				break;
			case "Perez":
				commentLap.sergioperez = lap.drivers[j].commentsIta;
				break;
			case "Hulkenberg":
				commentLap.nicohulkenberg = lap.drivers[j].commentsIta;
				break;
			case "Verstappen":
				commentLap.maxverstappen = lap.drivers[j].commentsIta;
				break;
			case "Sainz":
				commentLap.carlossainz = lap.drivers[j].commentsIta;
				break;
			case "Grosjean":
				commentLap.romaingrosjean = lap.drivers[j].commentsIta;
				break;
			case "Maldonado":
				commentLap.pastormaldonado = lap.drivers[j].commentsIta;
				break;
			case "Nasr":
				commentLap.felipenasr = lap.drivers[j].commentsIta;
				break;
			case "Ericson":
				commentLap.marcusericsson = lap.drivers[j].commentsIta;
				break;
			case "Stevens":
				commentLap.willstevens = lap.drivers[j].commentsIta;
				break;
			case "Merhi":
				commentLap.robertomerhi = lap.drivers[j].commentsIta;
				break;
			default:
				break;
			}
		}
		return commentLap;
	}
	
	public static IREventsComments setCommentDriverEng(IREventsComments commentLap, Lap lap) 
	{
		for(int j=0; j<lap.drivers.length;j++)
		{
			switch (lap.drivers[j].name) 
			{
			case "Hamilton":
				commentLap.lewishamilton = lap.drivers[j].commentsEng;
				break;
			case "Rosberg":
				commentLap.nicorosberg = lap.drivers[j].commentsEng;
				break;
			case "Ricciardo":
				commentLap.danielricciardo = lap.drivers[j].commentsEng;
				break;
			case "Kvyat":
				commentLap.daniilkvyat = lap.drivers[j].commentsEng;
				break;
			case "Massa":
				commentLap.felipemassa = lap.drivers[j].commentsEng;
				break;
			case "Bottas":
				commentLap.valtteribottas = lap.drivers[j].commentsEng;
				break;
			case "Vettel":
				commentLap.sebastianvettel = lap.drivers[j].commentsEng;
				break;
			case "Raikkonen":
				commentLap.kimiraikkonen = lap.drivers[j].commentsEng;
				break;
			case "Button":
				commentLap.jensonbutton = lap.drivers[j].commentsEng;
				break;
			case "Alonso":
				commentLap.fernandoalonso = lap.drivers[j].commentsEng;
				break;
			case "Perez":
				commentLap.sergioperez = lap.drivers[j].commentsEng;
				break;
			case "Hulkenberg":
				commentLap.nicohulkenberg = lap.drivers[j].commentsEng;
				break;
			case "Verstappen":
				commentLap.maxverstappen = lap.drivers[j].commentsEng;
				break;
			case "Sainz":
				commentLap.carlossainz = lap.drivers[j].commentsEng;
				break;
			case "Grosjean":
				commentLap.romaingrosjean = lap.drivers[j].commentsEng;
				break;
			case "Maldonado":
				commentLap.pastormaldonado = lap.drivers[j].commentsEng;
				break;
			case "Nasr":
				commentLap.felipenasr = lap.drivers[j].commentsEng;
				break;
			case "Ericson":
				commentLap.marcusericsson = lap.drivers[j].commentsEng;
				break;
			case "Stevens":
				commentLap.willstevens = lap.drivers[j].commentsEng;
				break;
			case "Merhi":
				commentLap.robertomerhi = lap.drivers[j].commentsEng;
				break;
			default:
				break;
			}
		}
		return commentLap;
	}

	public static IRLapPosition setDriverPosition(IRLapPosition positionLap, Lap gara) 
	{
		for(int j=0; j<gara.drivers.length;j++)
		{
			switch (gara.drivers[j].name) 
			{
			case "Hamilton":
				positionLap.lewishamilton = gara.drivers[j].position;
				break;
			case "Rosberg":
				positionLap.nicorosberg = gara.drivers[j].position;
				break;
			case "Ricciardo":
				positionLap.danielricciardo = gara.drivers[j].position;
				break;
			case "Kvyat":
				positionLap.daniilkvyat = gara.drivers[j].position;
				break;
			case "Massa":
				positionLap.felipemassa = gara.drivers[j].position;
				break;
			case "Bottas":
				positionLap.valtteribottas = gara.drivers[j].position;
				break;
			case "Vettel":
				positionLap.sebastianvettel = gara.drivers[j].position;
				break;
			case "Raikkonen":
				positionLap.kimiraikkonen = gara.drivers[j].position;
				break;
			case "Button":
				positionLap.jensonbutton = gara.drivers[j].position;
				break;
			case "Alonso":
				positionLap.fernandoalonso = gara.drivers[j].position;
				break;
			case "Perez":
				positionLap.sergioperez = gara.drivers[j].position;
				break;
			case "Hulkenberg":
				positionLap.nicohulkenberg = gara.drivers[j].position;
				break;
			case "Verstappen":
				positionLap.maxverstappen = gara.drivers[j].position;
				break;
			case "Sainz":
				positionLap.carlossainz = gara.drivers[j].position;
				break;
			case "Grosjean":
				positionLap.romaingrosjean = gara.drivers[j].position;
				break;
			case "Maldonado":
				positionLap.pastormaldonado = gara.drivers[j].position;
				break;
			case "Nasr":
				positionLap.felipenasr = gara.drivers[j].position;
				break;
			case "Ericson":
				positionLap.marcusericsson = gara.drivers[j].position;
				break;
			case "Stevens":
				positionLap.willstevens = gara.drivers[j].position;
				break;
			case "Merhi":
				positionLap.robertomerhi = gara.drivers[j].position;
				break;
			default:
				break;
			}
		}
		return positionLap;	
	}
	
	public static IREventsComments setEventDriver(IREventsComments eventLap,Lap lap) 
	{
		for(int j=0; j<lap.drivers.length;j++)
		{
			switch (lap.drivers[j].name) 
			{
			case "Hamilton":
				eventLap.lewishamilton = lap.drivers[j].events;
				break;
			case "Rosberg":
				eventLap.nicorosberg = lap.drivers[j].events;
				break;
			case "Ricciardo":
				eventLap.danielricciardo = lap.drivers[j].events;
				break;
			case "Kvyat":
				eventLap.daniilkvyat = lap.drivers[j].events;
				break;
			case "Massa":
				eventLap.felipemassa = lap.drivers[j].events;
				break;
			case "Bottas":
				eventLap.valtteribottas = lap.drivers[j].events;
				break;
			case "Vettel":
				eventLap.sebastianvettel = lap.drivers[j].events;
				break;
			case "Raikkonen":
				eventLap.kimiraikkonen = lap.drivers[j].events;
				break;
			case "Button":
				eventLap.jensonbutton = lap.drivers[j].events;
				break;
			case "Alonso":
				eventLap.fernandoalonso = lap.drivers[j].events;
				break;
			case "Perez":
				eventLap.sergioperez = lap.drivers[j].events;
				break;
			case "Hulkenberg":
				eventLap.nicohulkenberg = lap.drivers[j].events;
				break;
			case "Verstappen":
				eventLap.maxverstappen = lap.drivers[j].events;
				break;
			case "Sainz":
				eventLap.carlossainz = lap.drivers[j].events;
				break;
			case "Grosjean":
				eventLap.romaingrosjean = lap.drivers[j].events;
				break;
			case "Maldonado":
				eventLap.pastormaldonado = lap.drivers[j].events;
				break;
			case "Nasr":
				eventLap.felipenasr = lap.drivers[j].events;
				break;
			case "Ericson":
				eventLap.marcusericsson = lap.drivers[j].events;
				break;
			case "Stevens":
				eventLap.willstevens = lap.drivers[j].events;
				break;
			case "Merhi":
				eventLap.robertomerhi = lap.drivers[j].events;
				break;
			default:
				break;
			}
		}
		return eventLap;
	}
}
