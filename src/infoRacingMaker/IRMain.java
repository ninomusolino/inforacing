package infoRacingMaker;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.BorderFactory;
import javax.swing.DefaultCellEditor;
import javax.swing.DropMode;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;

import java.awt.BorderLayout;

import javax.swing.JButton;

import java.awt.event.ActionEvent;

import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumn;

import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.ArrayList;
import java.awt.Font;

import javax.swing.SwingConstants;
import javax.swing.JToolBar;

public class IRMain
{
	private String workingDirectory;
	private JFrame frame;
	private JTable table;
	private JLabel labelLap;
	private JButton btnNewButton;
	private JButton btnBackButton;
	private JButton btnCloseGP;
	private Lap[] race;
	private Lap lap;
	private Driver[] driverList;
	private int currentLap;
	private int totalLap;
	private int currentRow;
	private int currentColoumn;
	private Color customOrange = new Color(232, 102, 0);
	private Color customGreen = new Color(0, 207, 28);
	private Color customYellow = new Color(255, 208, 0);
	static private String[] DriverName = { "Hamilton", "Rosberg", "Ricciardo",
			"Kvyat", "Massa", "Bottas", "Vettel", "Raikkonen", "Button",
			"Alonso", "Perez", "Hulkenberg", "Verstappen", "Sainz", "Grosjean",
			"Maldonado", "Nasr", "Ericson", "Stevens", "Merhi" };
	private int[] pitstopNumber;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					UIManager.setLookAndFeel(UIManager
							.getSystemLookAndFeelClassName());
					IRMain window = new IRMain();
					window.frame.setVisible(true);
				}
				catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public IRMain()
	{
		currentLap = 0;
		currentColoumn = 0;
		currentRow = 0;
		pitstopNumber = new int[20];
		new ArrayList<Lap>();
		driverList = new Driver[20];
		driverList = createDriver();
		lap = new Lap(driverList, "", "", "", 0);
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frame = new JFrame();
		frame.setBounds(100, 100, 1116, 637);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("File");
		menuBar.add(mnNewMenu);

		JMenuItem mntmNew = new JMenuItem("Nuovo GP");
		mntmNew.setToolTipText("Avvia un nuovo GP");
		mntmNew.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				frame.dispose();
				IRMain window = new IRMain();
				window.frame.setVisible(true);
			}
		});
		mnNewMenu.add(mntmNew);

		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.setToolTipText("Esci dall'applicazione");
		mntmExit.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				System.exit(0);
			}
		});
		mnNewMenu.add(mntmExit);
		frame.getContentPane();
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		labelLap = new JLabel("Griglia di Partenza");
		labelLap.setHorizontalAlignment(SwingConstants.CENTER);
		labelLap.setFont(new Font("Lucida Grande", Font.BOLD, 14));
		panel.add(labelLap, BorderLayout.NORTH);
		Border blackline = BorderFactory.createLineBorder(Color.black);
		btnCloseGP = new JButton("Termina GP");
		btnBackButton = new JButton("Giro Precedente");
		if (currentLap == 0)
		{
			btnBackButton.setVisible(false);
			btnCloseGP.setVisible(false);
		}

		btnCloseGP.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				try
				{
					IRJsonWriter
							.printIRLapPosition(race, workingDirectory);
					IRJsonWriter.printIREvents(race, workingDirectory);
					IRJsonWriter
							.printIRComments(race, workingDirectory);
				}
				catch (IOException e1)
				{
					e1.printStackTrace();
				}
			}
		});

		btnBackButton.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				// controllo perchè il giro corrente diventa maggiore a fine GP
				if (currentLap > totalLap)
				{
					btnCloseGP.setVisible(true);
					currentLap--;
				}
				if(currentLap==totalLap)
					btnNewButton.setText("Prossimo Giro");
				// SALVO
				IRTableModel currentModel = (IRTableModel) table.getModel();
				Lap lapToSave = new Lap(currentModel.getCurrentLap());
				Driver[] currentDriverList = new Driver[20];
				for (int i = 0; i < 20; i++)
				{
					currentDriverList[i] = new Driver();
					currentDriverList[i].name = lapToSave.drivers[i].name;
					currentDriverList[i].position = lapToSave.drivers[i].position;
					currentDriverList[i].commentsIta = lapToSave.drivers[i].commentsIta;
					currentDriverList[i].commentsEng = lapToSave.drivers[i].commentsEng;
					currentDriverList[i].events = lapToSave.drivers[i].events;
					lapToSave.drivers[i].events = "";
					lapToSave.drivers[i].commentsIta = "";
					lapToSave.drivers[i].commentsEng = "";
				}

				race[currentLap] = new Lap(currentDriverList,
						lapToSave.statusEvents, lapToSave.statusIta,
						lapToSave.statusEng, lapToSave.currentLap);
				// FINE SALVATAGGIO
				currentLap--;
				if (currentLap == 0)
				{
					btnBackButton.setVisible(false);
					labelLap.setText("Griglia di Partenza");
				}
				else
				{
					labelLap.setText("" + currentLap + "° Giro");
					if (!btnNewButton.isEnabled())
					{
						btnNewButton.setText("Prossimo Giro");
						btnNewButton.setEnabled(true);
					}
				}
				Lap previousLap = race[currentLap];
				IRTableModel newModel = new IRTableModel(previousLap);
				table.setModel(newModel);
				newModel.fireTableDataChanged();
				setColumnTableWidth();
			}
		});
		btnNewButton = new JButton("Inizia il GP!");
		btnNewButton.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				// System.out.println("CLICK");
				if (currentLap <= totalLap)
				{

					IRTableModel currentModel = (IRTableModel) table.getModel();
					Lap previousLap = new Lap(currentModel.getCurrentLap());
					Driver[] currentDriverList = new Driver[20];
					for (int i = 0; i < 20; i++)
					{
						currentDriverList[i] = new Driver();
						currentDriverList[i].name = previousLap.drivers[i].name;
						currentDriverList[i].position = previousLap.drivers[i].position;
						currentDriverList[i].commentsIta = previousLap.drivers[i].commentsIta;
						currentDriverList[i].commentsEng = previousLap.drivers[i].commentsEng;
						currentDriverList[i].events = previousLap.drivers[i].events;
						previousLap.drivers[i].events = "";
						previousLap.drivers[i].commentsIta = "";
						previousLap.drivers[i].commentsEng = "";
					}

					race[currentLap] = new Lap(currentDriverList,
							previousLap.statusEvents, previousLap.statusIta,
							previousLap.statusEng, previousLap.currentLap);
					currentLap++;
					if (currentLap > 0)
						btnBackButton.setVisible(true);
					if (currentLap > totalLap)
					{
						btnNewButton.setText("GP Concluso!");
						btnNewButton.setEnabled(false);
						try
						{
							IRJsonWriter
									.printIRLapPosition(race, workingDirectory);
							IRJsonWriter.printIREvents(race, workingDirectory);
							IRJsonWriter
									.printIRComments(race, workingDirectory);
						}
						catch (IOException e1)
						{
							e1.printStackTrace();
						}
					}
					else
					{
						if (currentLap == totalLap)
						{
							btnCloseGP.setVisible(false);
							btnNewButton.setText("Termina il GP");
						}
						else
						{
							btnNewButton.setText("Prossimo giro");
						}
						labelLap.setText("" + currentLap + "° Giro");
						if (race[currentLap] != null)
						{
							Lap lapExistent = race[currentLap];
							IRTableModel existentModel = new IRTableModel(
									lapExistent);
							table.setModel(existentModel);
							existentModel.fireTableDataChanged();
						}
						else
						{
							lap = new Lap(previousLap.drivers,
									previousLap.statusEvents,
									previousLap.statusIta,
									previousLap.statusEng, currentLap);
							IRTableModel nextModel = new IRTableModel(lap);
							table.setModel(nextModel);
							nextModel.fireTableDataChanged();
						}
						setColumnTableWidth();
					}
				}
			}
		});

		table = new JTable(new IRTableModel(lap));

		// draggable cell
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setDragEnabled(true);
		table.setDropMode(DropMode.USE_SELECTION);
		table.setTransferHandler(new TableRowTransferHandler());
		table.setShowGrid(true);
		table.setGridColor(Color.gray);
		table.setBorder(blackline);
		table.setCellSelectionEnabled(true);
		setColumnTableWidth();
		JScrollPane scroll = new JScrollPane(table);
		panel.add(scroll, BorderLayout.CENTER);

		// Fiddle with the Sport column's cell editors/renderers.
		// setUpCommentsItaColumn(table, table.getColumnModel().getColumn(2));
		JPanel buttonContainerSud = new JPanel();
		panel.add(buttonContainerSud, BorderLayout.SOUTH);
		buttonContainerSud.add(btnBackButton);
		buttonContainerSud.add(btnNewButton);
		buttonContainerSud.add(btnCloseGP);
		MouseListener buttonMouseListener = new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{

				Object o = e.getSource();
				JButton b = null;
				String buttonText = "";

				if (o instanceof JButton)
					b = (JButton) o;
				if (b != null)
					buttonText = b.getText();

				if (currentColoumn == 2)
				{
					if (currentRow == 20 && (buttonText.equals("safety") || buttonText
							.equals("start-rain") || buttonText
								.equals("start-rain")))
						table.setValueAt(buttonText, currentRow, currentColoumn);
					else
						if (currentRow < 20)
						{
							if (buttonText.equals("Clear"))
							{
								if (table
										.getValueAt(currentRow, currentColoumn)
										.toString().contains("pitstop"))
								{
									removePitStopFromHistory(currentRow);
								}
								table.setValueAt("", currentRow, 3);
								table.setValueAt("", currentRow, 4);
								table.setValueAt("", currentRow, currentColoumn);
							}
							else
							{
								if ((!buttonText.equals("safety") && !buttonText
										.equals("start-rain") && !buttonText
										.equals("stop-rain")))
								{
									if (buttonText.toLowerCase()
											.contains("pitstop"))
									{
										if (table
												.getValueAt(currentRow, currentColoumn)
												.toString().contains("pitstop"))
										{
											removePitStopFromHistory(currentRow);
										}
										String commentItaPit = computePitstopNumber(currentRow, buttonText);
										table.setValueAt(commentItaPit, currentRow, 3);
										table.setValueAt(commentItaPit, currentRow, 4);
										table.setValueAt(buttonText, currentRow, currentColoumn);
									}
									else
									{
										String[] commentsEvent = computeEventComment(buttonText);
										String commentItaEvent = commentsEvent[0];
										String commentEngEvent = commentsEvent[1];
										table.setValueAt(commentItaEvent, currentRow, 3);
										table.setValueAt(commentEngEvent, currentRow, 4);
										table.setValueAt(buttonText, currentRow, currentColoumn);
									}
								}
								else
									JOptionPane
											.showMessageDialog(null, "Non puoi assegnare un evento di una vettura allo status della gara!");
							}
						}
					IRTableModel currentModel = (IRTableModel) table.getModel();
					currentModel.fireTableDataChanged();
					setColumnTableWidth();
				}
				else
				{
					JOptionPane
							.showMessageDialog(null, "Seleziona prima la colonna degli eventi!");
				}
			}

		};

		JPanel panel_1 = new JPanel();
		frame.getContentPane().add(panel_1, BorderLayout.NORTH);

		JPanel buttonContainer = new JPanel();
		JToolBar toolBar = new JToolBar();
		panel_1.add(toolBar);
		toolBar.add(buttonContainer);

		JButton btnClear = new JButton("Clear");
		btnClear.setForeground(Color.BLACK);
		btnClear.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnClear);

		JButton btnP = new JButton("pitstop-ss");
		btnP.setForeground(Color.RED);
		btnP.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnP);

		JButton btnPitstops = new JButton("pitstop-s");
		btnPitstops.setForeground(customYellow);
		btnPitstops.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnPitstops);

		JButton btnPitstopm = new JButton("pitstop-m");
		btnPitstopm.setForeground(Color.DARK_GRAY);
		btnPitstopm.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnPitstopm);

		JButton btnPitstoph = new JButton("pitstop-h");
		btnPitstoph.setForeground(customOrange);
		btnPitstoph.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnPitstoph);

		JButton btnPitstopi = new JButton("pitstop-i");
		btnPitstopi.setForeground(customGreen);
		btnPitstopi.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnPitstopi);

		JButton btnPitstopw = new JButton("pitstop-w");
		btnPitstopw.setForeground(Color.BLUE);
		btnPitstopw.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnPitstopw);

		JButton btnPitOut = new JButton("pitout");
		btnPitOut.setForeground(Color.RED);
		btnPitOut.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnPitOut);

		JButton btnWarmBreak = new JButton("warmbreak");
		btnWarmBreak.setForeground(Color.RED);
		btnWarmBreak.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnWarmBreak);

		JButton btnWarmout = new JButton("warmout");
		btnWarmout.setForeground(Color.RED);
		btnWarmout.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnWarmout);

		JButton btnBreak = new JButton("break");
		btnBreak.setForeground(Color.RED);
		btnBreak.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnBreak);

		JButton btnBreakout = new JButton("breakout");
		btnBreakout.setForeground(Color.RED);
		btnBreakout.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnBreakout);

		JButton btnCrash = new JButton("crash");
		btnCrash.setForeground(Color.RED);
		btnCrash.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnCrash);

		JButton btnCrashout = new JButton("crashout");
		btnCrashout.setForeground(Color.RED);
		btnCrashout.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnCrashout);

		JButton btnSafetyCar = new JButton("safety");
		btnSafetyCar.setForeground(Color.RED);
		btnSafetyCar.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnSafetyCar);

		JButton btnStartRain = new JButton("start-rain");
		btnStartRain.setForeground(Color.RED);
		btnStartRain.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnStartRain);

		JButton btnStopRain = new JButton("stop-rain");
		btnStopRain.setForeground(Color.RED);
		btnStopRain.addMouseListener(buttonMouseListener);
		buttonContainer.add(btnStopRain);

		String raceLap = "a";
		while (!isNumeric(raceLap))
		{
			raceLap = (String) JOptionPane
					.showInputDialog(frame, "Inserisci il numero di Giri:", "Tiwi InfoRacing", JOptionPane.PLAIN_MESSAGE, null, null, "");
		}
		totalLap = Integer.parseInt(raceLap);
		race = new Lap[totalLap + 1];
		getOS();

		KeyListener tableKeyListener = new KeyAdapter()
		{
			@Override
			public void keyReleased(KeyEvent e)
			{
				if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_UP)
				{
					if (table.getSelectedColumn() == 2)
					{
						currentRow = table.getSelectedRow();
						currentColoumn = table.getSelectedColumn();
					}
				}
			}
		};

		MouseListener tableMouseListener = new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				if (table.getSelectedColumn() == 2)
				{
					currentRow = table.rowAtPoint(e.getPoint());// get
					// mouse-selected
					// row
					currentColoumn = table.columnAtPoint(e.getPoint());// get
					// mouse-selected
					// col
				}
			}
		};
		table.addMouseListener(tableMouseListener);
		table.addKeyListener(tableKeyListener);
	}

	private void setColumnTableWidth()
	{
		DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
		centerRenderer.setHorizontalAlignment(JLabel.CENTER);
		table.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
		table.getColumnModel().getColumn(0).setMinWidth(90);
		table.getColumnModel().getColumn(0).setMaxWidth(90);
		table.getColumnModel().getColumn(0).setPreferredWidth(90);
		table.getColumnModel().getColumn(1).setMinWidth(90);
		table.getColumnModel().getColumn(1).setMaxWidth(90);
		table.getColumnModel().getColumn(1).setPreferredWidth(90);
		table.getColumnModel().getColumn(2).setMinWidth(100);
		table.getColumnModel().getColumn(2).setMaxWidth(100);
		table.getColumnModel().getColumn(2).setPreferredWidth(100);
	}

	protected void removePitStopFromHistory(int currentRow)
	{
		String driver = (String) table.getValueAt(currentRow, 1);
		int index = getDriverIndex(driver);
		pitstopNumber[index]--;
	}

	protected String[] computeEventComment(String buttonText)
	{
		String[] comments = new String[2];
		switch (buttonText)
			{
			case "crash":
				comments[0] = "Contatto con";
				comments[1] = "Collision with";
				break;
			case "crashout":
				comments[0] = "Ritiro per contatto con";
				comments[1] = "Retiring due to collision with";
				break;
			case "break":
				comments[0] = "Problema con";
				comments[1] = "Issue with";
				break;
			case "breakout":
				comments[0] = "Ritiro per problemi a";
				comments[1] = "Retiring due to";
				break;
			case "pitout":
				comments[0] = "Ritiro ai box";
				comments[1] = "Retiring into the box";
				break;
			default:
				comments[0] = "";
				comments[1] = "";
				break;
			}
		return comments;
	}

	protected String computePitstopNumber(int currentRow, String btnText)
	{
		String comment = "";
		String driver = (String) table.getValueAt(currentRow, 1);
		int index = getDriverIndex(driver);
		pitstopNumber[index]++;
		int numPit = pitstopNumber[index];
		String tyreType = getTyreType(btnText);
		comment = "PIT" + numPit + "-" + tyreType;
		return comment;
	}

	private String getTyreType(String btnText)
	{
		String croppedString;
		croppedString = btnText.split("-")[1];
		return croppedString.toUpperCase();
	}

	private void getOS()
	{
		String OS = (System.getProperty("os.name")).toUpperCase();
		if (OS.contains("WIN"))
		{
			workingDirectory = System.getenv("Desktop");
			workingDirectory += "\\";
		}
		else
		{
			workingDirectory = System.getProperty("user.home");
			workingDirectory += "/Desktop/";
		}
	}

	public static boolean isNumeric(String str)
	{
		if (str.isEmpty())
		{
			return false;
		}
		NumberFormat formatter = NumberFormat.getInstance();
		ParsePosition pos = new ParsePosition(0);
		formatter.parse(str, pos);
		return str.length() == pos.getIndex();
	}

	/*
	 * private void setUpCommentsItaColumn(JTable table2, TableColumn column) {
	 * Color[] colors = {Color.black, Color.black, Color.black,
	 * Color.black,Color
	 * .black,Color.black,Color.black,Color.black,Color.black,Color
	 * .black,Color.black,Color.black,Color.black}; String[] strings = {"",
	 * "pitstop","pitstop-ss",
	 * "pitstop-s","pitstop-m","pitstop-h","pitstop-i","pitstop-w"
	 * ,"warmout","break","breakout","crash","crashout"}; JComboBox<?> cmb = new
	 * JComboBox<Object>(strings); ComboBoxRenderer rendererC = new
	 * ComboBoxRenderer(cmb);
	 * 
	 * rendererC.setColors(colors); rendererC.setStrings(strings);
	 * 
	 * cmb.setRenderer(rendererC);
	 * 
	 * //Set up the editor for the sport cells. JComboBox<String> comboBox = new
	 * JComboBox<String>(); comboBox.addItem("pitstop");
	 * comboBox.addItem("pitstop-s"); comboBox.addItem("pitstop-m");
	 * comboBox.addItem("pitstop-h"); comboBox.addItem("pitstop-w");
	 * comboBox.addItem("warmout"); comboBox.addItem("break");
	 * comboBox.addItem("breakout"); comboBox.addItem("crash");
	 * comboBox.addItem("crashout"); comboBox.addItem("");
	 * column.setCellEditor(new DefaultCellEditor(cmb)); //Set up tool tips for
	 * the sport cells. DefaultTableCellRenderer renderer = new
	 * DefaultTableCellRenderer(); renderer.setToolTipText("Scegli un evento");
	 * column.setCellRenderer(renderer); }
	 */

	private Driver[] createDriver()
	{
		Driver[] result = new Driver[20];

		for (int i = 0; i < 20; i++)
		{
			result[i] = new Driver();
			result[i].name = DriverName[i];
			result[i].position = i + 1;
			result[i].events = "";
			result[i].commentsIta = "";
			result[i].commentsEng = "";
		}
		return result;
	}

	private int getDriverIndex(String driver)
	{
		int index = 0;
		switch (driver)
			{
			case "Hamilton":
				index = 0;
				break;
			case "Rosberg":
				index = 1;
				break;
			case "Ricciardo":
				index = 2;
				break;
			case "Kvyat":
				index = 3;
				break;
			case "Massa":
				index = 4;
				break;
			case "Bottas":
				index = 5;
				break;
			case "Vettel":
				index = 6;
				break;
			case "Raikkonen":
				index = 7;
				break;
			case "Button":
				index = 8;
				break;
			case "Alonso":
				index = 9;
				break;
			case "Perez":
				index = 10;
				break;
			case "Hulkenberg":
				index = 11;
				break;
			case "Verstappen":
				index = 12;
				break;
			case "Sainz":
				index = 13;
				break;
			case "Grosjean":
				index = 14;
				break;
			case "Maldonado":
				index = 15;
				break;
			case "Nasr":
				index = 16;
				break;
			case "Ericson":
				index = 17;
				break;
			case "Stevens":
				index = 18;
				break;
			case "Merhi":
				index = 19;
				break;
			default:
				break;
			}
		return index;
	}
}
