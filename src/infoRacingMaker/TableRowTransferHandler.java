package infoRacingMaker;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.io.IOException;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.TransferHandler;


/**
 * Handles drag & drop row reordering
 */
public class TableRowTransferHandler extends TransferHandler 
{

	private int startRow;
	private int startCol;
	private String startName;
	
	private static final long serialVersionUID = 1L;

	public TableRowTransferHandler() {
		super();
	}

	public TableRowTransferHandler(String property) {
		super(property);
	}

	public int getSourceActions(JComponent c) {
        return DnDConstants.ACTION_COPY_OR_MOVE;
    }

    public Transferable createTransferable(JComponent comp)
    {
        JTable table = (JTable)comp;
        startRow=table.getSelectedRow();
        startCol=table.getSelectedColumn();
        if(startCol!=1 || startRow==20)
        {
        	return null;
        }
        else
        {
        	startName = (String)table.getModel().getValueAt(startRow,startCol);
        	StringSelection transferable = new StringSelection(startName);
        	return transferable;
        }
    }
    public boolean canImport(TransferHandler.TransferSupport info)
    {
        if (!info.isDataFlavorSupported(DataFlavor.stringFlavor))
        {
            return false;
        }
        info.setShowDropLocation(true);
        return true;
    }

    public boolean importData(TransferSupport support) 
    {
    	
        JTable table=(JTable)support.getComponent();
    	IRTableModel tableModel=(IRTableModel)table.getModel();
        JTable.DropLocation dl = (JTable.DropLocation)support.getDropLocation();
        int row = dl.getRow();
        int col=dl.getColumn();
        
    	if(col!=1)
    	{
    		return false;
    	}
        if (!support.isDrop()) 
        {
            return false;
        }

        if (!canImport(support)) 
        {
            return false;
        }

        String data;
        try {
            data = (String)support.getTransferable().getTransferData(DataFlavor.stringFlavor);
        } catch (UnsupportedFlavorException e) {
            return false;
        } catch (IOException e) {
            return false;
        }
        
        if(row<startRow)
        {
        	//System.out.println("start Row "+startRow+" e riga "+row+" Riga maggiore");
        	tableModel.shiftPositionUP(data, row);
        }
        else
        {
        	//System.out.println("start Row "+startRow+" e riga "+row+" Riga minore");
        	tableModel.shiftPositionDOWN(data,row);
        }
        return true;
        
        //System.out.println(data);
       //System.out.println("Selezionato "+startName);
        //String tempName = (String)tableModel.getValueAt(row, col);
        //System.out.println("da sostituire "+tempName+" alla colonna "+startCol+" e riga "+startRow);
        //tableModel.setValueAt(tempName,startRow,startCol);
        //tableModel.setValueAt(data, row, col);
        //tableModel.fireTableDataChanged();
        
    }
}