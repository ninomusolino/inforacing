package infoRacingMaker;

public class Lap 
{
	public Driver drivers[];
	public String statusEvents;
	public String statusIta;
	public String statusEng;
	public int currentLap;
	
	public Lap(Driver[] drivers, String statusEvents, String statusIta,
			String statusEng, int currentLap) {
		super();
		this.drivers = drivers;
		this.statusEvents = statusEvents;
		this.statusIta = statusIta;
		this.statusEng = statusEng;
		this.currentLap = currentLap;
	}

	public Lap(Lap currentLap2) 
	{
		this.drivers = currentLap2.drivers;
		this.statusEvents = currentLap2.statusEvents;
		this.statusIta = currentLap2.statusIta;
		this.statusEng = currentLap2.statusEng;
		this.currentLap = currentLap2.currentLap;
	}

	public Lap() {
		// Only for Testing
	}

	public Driver[] getDrivers() {
		return drivers;
	}


	public void setDrivers(Driver[] drivers) {
		this.drivers = drivers;
	}

	public String getStatusIta() {
		return statusIta;
	}

	public void setStatusIta(String statusIta) {
		this.statusIta = statusIta;
	}

	public String getStatusEng() {
		return statusEng;
	}

	public void setStatusEng(String statusEng) {
		this.statusEng = statusEng;
	}

	public int getCurrentLap() {
		return currentLap;
	}

	public void setCurrentLap(int currentLap) {
		this.currentLap = currentLap;
	}

	public String getStatusEvents() {
		return statusEvents;
	}

	public void setStatusEvents(String statusEvents) {
		this.statusEvents = statusEvents;
	}
	
}
